using Godot;

public class Cell
{
    public Cell(int type, Vector2I position)
    {
        EnvironmentType = type;
        Position = position;
    }
    public int EnvironmentType;
    public int OtherType = 0;
    public Vector2I Position;
    public Vector2I NoiseOffset = Vector2I.Zero;
   
}