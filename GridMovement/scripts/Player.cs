using System.Collections.Generic;
using Godot;

public class Player
{
    public Vector2I Position {get; private set;} = Vector2I.Zero;

    public void Move(Vector2I delta)
    {
        
        Position += delta;
    }

}
   