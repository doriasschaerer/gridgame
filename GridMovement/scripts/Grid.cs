using Godot;
using Godot.Bridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public partial class Grid : Node
{
	[Export]
	private int _Resolution;
	private Vector2I _dimensions;

	[Export]
	private int _CellType;

	[Export] private ShaderMaterial _EnvironmentRendererMaterial;
	[Export] private ShaderMaterial _OtherRendererMaterial;
	
	private Cell[,] _cells;
	private RandomNumberGenerator _random;


	[Export] 
	private FastNoiseLite _noise;

	[Export]
	private Curve _TileDistribution;

	private Image _environmentMapImage;
	private Image _otherMapImage;
	private int _tileAmountEnvironment;
	private int _tileAmountOther;
	private bool _gameStarted = false;
	private Vector2I _currentScreenOffset = Vector2I.Zero;
	private Vector2I _currentScaledScreenOffset = Vector2I.Zero;
	private Player _player;
	private HashSet<Cell> _playerAdjacentCells;
	private HashSet<Cell> _allCells;
	private HashSet<Enemy> _enemies;

	private float _offset = 0;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_player = new();
		_player.Move(new Vector2I(3,3));
		
		_random = new();
		_random.Randomize();
		
		Vector2 mapDimensionsEnvironment = (Vector2)_EnvironmentRendererMaterial.GetShaderParameter("mapDimensions");
		Vector2 mapDimensionsOther = (Vector2)_OtherRendererMaterial.GetShaderParameter("mapDimensions");

		_tileAmountEnvironment = (int)mapDimensionsEnvironment.X;
		_tileAmountOther = (int)mapDimensionsOther.X;

		_dimensions = new Vector2I(16, 9) * _Resolution;
		_EnvironmentRendererMaterial.SetShaderParameter("resolution", _Resolution);
		_OtherRendererMaterial.SetShaderParameter("resolution", _Resolution);

		_cells = new Cell[_dimensions.X, _dimensions.Y];

		_environmentMapImage = Image.Create(_dimensions.X, _dimensions.Y, false, Image.Format.Rgb8);
		_otherMapImage = Image.Create(_dimensions.X, _dimensions.Y, false, Image.Format.Rgb8);

		InitCells();
		
		UpdateEnvironmentGrid(_offset);

		_allCells = new();
		foreach(Cell cell in _cells) _allCells.Add(cell);
		DrawAllGridLayers(_allCells);
		//GenerateOtherGrid();
	}



    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _PhysicsProcess(double delta)
	{
		SetOffset((float)delta);
		MoveCharacter();
		_playerAdjacentCells = GetAdjacentCells(_player.Position);
		UpdateOtherGrid(_playerAdjacentCells);
		DrawAllGridLayers(_playerAdjacentCells);
	}



	private void InitCells()
	{
		for (int i = 0; i < _dimensions.X; i++)
		{
			for (int j = 0; j < _dimensions.Y; j++)
			{
				_cells[i, j] = new(0, new Vector2I(i, j));
			}
		}
	}

	private HashSet<Cell> GetAdjacentCells(Vector2I position)
	{
		HashSet<Cell> adjacentCells = new();
		Vector2I[] offsets = new Vector2I[9] {Vector2I.Zero, Vector2I.Up, Vector2I.Down, Vector2I.Left, Vector2I.Right, Vector2I.One, Vector2I.One * -1, new(1, -1), new(-1, 1)};
		foreach (Vector2I offset in offsets)
		{
			Vector2I cellPos = position + offset;
			
			if (IsInBounds(cellPos))
			{
				cellPos -= _currentScaledScreenOffset;
				adjacentCells.Add(_cells[cellPos.X, cellPos.Y]);
			}
		}
		return adjacentCells;
	}

	private bool IsInBounds(Vector2I position)
	{
		Vector2I screenPosition = position - _currentScaledScreenOffset;
		
		return screenPosition.X >= 0 && screenPosition.X < _dimensions.X && screenPosition.Y >= 0 && screenPosition.Y < _dimensions.Y;
	}

	void MoveCharacter()
	{
		if (Input.IsActionPressed("Up")) 
		{
			_player.Move(Vector2I.Up); 
			CheckForScreenSwitch();
		}
		else if (Input.IsActionPressed("Down"))
		{
			_player.Move(Vector2I.Down); 
			CheckForScreenSwitch();
		}
		else if (Input.IsActionPressed("Left"))
		{
			_player.Move(Vector2I.Left); 
			CheckForScreenSwitch();
		}
		else if (Input.IsActionPressed("Right"))
		{
			_player.Move(Vector2I.Right); 
			CheckForScreenSwitch();
		}
	}

	void SetOffset(float delta)
	{
		if (Input.IsActionPressed("Offset")) 
		{
			_offset += delta * 5;
			UpdateEnvironmentGrid(_offset);
			DrawAllGridLayers(_allCells);
		}
	}

	private void CheckForScreenSwitch()
	{
		Vector2I screenOffset = _player.Position / _dimensions;
		if (_player.Position.X < 0) screenOffset += Vector2I.Left;
		if (_player.Position.Y < 0) screenOffset += Vector2I.Up;

		if (screenOffset != _currentScreenOffset)
		{
			UpdateCellPositions(screenOffset - _currentScreenOffset);
			
			_currentScreenOffset = screenOffset;
			_currentScaledScreenOffset = screenOffset * _dimensions;
			
			UpdateEnvironmentGrid(_offset);
			UpdateOtherGrid(_allCells);
			DrawAllGridLayers(_allCells);
		}
	}

	private void GenerateOtherGrid()
	{
		foreach (Cell currentCell in _cells)
		{
			if (currentCell.EnvironmentType == 6)
			{
				if (_random.RandiRange(0,5) == 0)
				{
					currentCell.OtherType = 3;
				}
			}
		}
	}

	private void UpdateCellPositions(Vector2I offset)
	{
		Vector2I scaledOffset = offset * _dimensions;
		foreach (Cell currentCell in _cells)
			currentCell.Position += scaledOffset;
	}

	private void UpdateEnvironmentGrid(float offsetZ)
	{
		foreach (Cell currentCell in _cells)
		{
			float c = _TileDistribution.Sample(_noise.GetNoise3Dv( new Vector3(currentCell.Position.X, currentCell.Position.Y, offsetZ)));
			c += 0.0001f;
			c = c * (float)_tileAmountEnvironment;

			currentCell.EnvironmentType = (int)c;
		}

		_gameStarted = true;
	}

	private void UpdateOtherGrid(HashSet<Cell> cells)
	{
		foreach (Cell currentCell in cells)
		{
			//GD.Print(currentCell.Position);
			if (currentCell.Position == _player.Position)
			{
				if (currentCell.EnvironmentType < 6)
				{
					currentCell.OtherType = 2;
				}
				else currentCell.OtherType = 1;
			}
			else if (!(currentCell.OtherType == 3))
			{
				currentCell.OtherType = 0;
			}
		}
	}

	private void DrawAllGridLayers(HashSet<Cell> cells)
	{	
		foreach (Cell currentCell in cells)
		{
			float c = (currentCell.EnvironmentType + 1) / (float)_tileAmountEnvironment;
			c -= 0.0001f;
			Color col = new Color(c, 0, 0);

			_environmentMapImage.SetPixelv(currentCell.Position - _currentScaledScreenOffset, col);

			c = (currentCell.OtherType + 1) / (float)_tileAmountOther;
			c -= 0.00001f;
			col = new Color(c, 0, 0);

			_otherMapImage.SetPixelv(currentCell.Position - _currentScaledScreenOffset, col);
		}

		ImageTexture environmentTexture = ImageTexture.CreateFromImage(_environmentMapImage);
		ImageTexture otherTexture = ImageTexture.CreateFromImage(_otherMapImage);

		_EnvironmentRendererMaterial.SetShaderParameter("inputTex", environmentTexture);
		_OtherRendererMaterial.SetShaderParameter("inputTex", otherTexture);
	}
}
