using System.Collections.Generic;
using Godot;

public class Enemy
{
    public Vector2I Position {get; set;} = Vector2I.Zero;

    public void Move(Vector2I delta)
    {
        
        Position += delta;
    }
}